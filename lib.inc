
section .text
exit: 	
    mov  rax, 60             ; функция exit
    xor  rdi, rdi            ;
    syscall   

;работает
string_length:
    xor rax,rax
    .count:
        cmp byte[rdi+rax], 0    ; поиск спец. символа
        je .end                 ; при нахождении - выход
        inc rax                 ; повторим цикл если не нашли
        jmp .count
    .end:
        ret

;работает
print_string:               
    call string_length      
    mov rsi, rdi            ; Переносим строку -> rsi
    mov rdx, rax            ; длина строки -> rdx
    mov rax, 1
    mov rdi, 1
    syscall 
    ret

;работает
print_char:
    push rdi
    mov  rax, 1    
    mov  rdx, 1       
    mov  rdi, 1         
    mov  rsi, rsp   
    syscall
    pop rax
    ret

;работает
print_newline:
    mov rdi, 0xA
    jmp print_char

;работает
print_uint:
    xor rdx, rdx                
    mov r13, rsp                ; сейвим положение стека
    dec rsp                     ; в стек пишем байт нуль-терминатор
    mov rax, rdi                ; теперь пишем в rax число для деления
    mov byte[rsp],dl
    .loop: 
        mov rcx, 0xA            ; деление числа в rdx:rax на 10   
        div rcx
        add rdx,'0'             ; цифру -> ASCII символ
        dec rsp                 ; цифра -> стек
        mov byte[rsp],dl
        xor rdx,rdx             
        cmp rax,0              
        ja .loop
    mov rdi,rsp                 ; вершина стека в качестве указателя на строку
    push r13                    ; сохраним старую вершину стека
    call print_string           ; отображение числа, как строки
    pop r13
    mov rsp,r13                 ; стек в исходное положение
    ret

;работает
print_int:
    cmp rdi, 0          ; число сравнивается с 0
    jnl print_uint      ; если положительное, то print_uint
    push rdi            ; rdi в стек
    mov rdi, '-'        ; минус в стек
    call print_char     ; отображаем его
    pop rdi             ; вернем rdi
    neg rdi             
    jmp print_uint      ; отображаем число

;работает
string_equals:
    xor rcx,rcx                 ; счетчик в 0
    .loop:
        mov r11b, byte[rsi+rcx] ; символ из первого слова
        cmp byte[rdi+rcx], r11b ; сравниваем с символом из 2 слова
        jne .false              ; 0, если не равны
        test r11b,r11b          ; Иначе при нахождении нуль-терминатора - 1
        jz .true 
        inc rcx
        jmp .loop
    .false:
        mov rax, 0
        ret
    .true:
        mov rax,1
        ret
       
;работает
read_char:
    dec rsp                     ; стек на один вверх
    mov byte[rsp], 0		; очистка
    mov rdi, 0                  ; stdin
    mov rax, 0                  ; ф-ия read
    mov rdx, 1                  ; читаем 1 символ
    mov rsi,rsp                 ; поместим как буффер ф-ии read    
    syscall
    mov al, byte[rsp]           ; буфер в аккум
    inc rsp                     ; стек в начальное положение
    ret 

;работает
read_word:
     xor rcx,rcx                ; счетчик в 0
    .loop:
        cmp rcx, rsi            ; текущая длина строки сравнивается с размером буффера
        jge .fail              
        push rsi
        push rdi                ; нужные регистры -> стек
        push rcx
        call read_char          ; читаем символ из потока
        pop rcx                 ; возврат регистров
        pop rdi
	pop rsi
        cmp al,0x20             ; прочитанный символ сравниваем с пробелом
        je .skip                ; равны - переходим в пропуск
        cmp al,0x9              ; прочитанный символ сравниваем с табуляцией
        je .skip
        cmp al,0xA              ; теперь сравниваем с переносом строки
        je .skip
        mov byte[rdi+rcx],al    ; символ -> буффер
        cmp al,0               
        je .success
        inc rcx                 
        jmp .loop               ; повтор цикла

    .skip:
        test rcx,rcx            ; длину проверяем на 0     
        jz .loop                ; если 0, продолжаем цикл
    .success:                   
         mov rdx, rcx            ; длина строки -> rdx
     	 mov rax, rdi            ; адрес буффера -> rax
        ret
    .fail:
        xor rax, rax            
        ret
 
;работает
parse_uint:
    xor     r11, r11         ; хранит количество байт в нашем числе
    xor     rdx, rdx       
    xor     rax, rax        
    mov     r10, 0xa       
    .loop:
        xor     r9, r9  
        mov     r9b, [rdi]  ; чтение символа
        inc     rdi         
        cmp     r9b, '0'    
        jb      .success
        cmp     r9b, '9'    
        ja      .success
        mul     r10         ; *10
        cmp     rdx, 0      ; проверяем, помещается ли в rax
        jne     .error
        sub     r9b, 0x30   ; символ -> число
        inc     r11         ; количество символов в числе увеличивается
   	add     rax, r9    
        jmp     .loop       ; все по новой
    .success:
        mov     rdx, r11   
        ret
    .error:
    	xor     rax, rax
        xor     rdx, rdx
        ret                                    
    
;работает
parse_int:
    cmp byte[rdi], '-'          ; проверяем, минус ли полученный символ
    je .negative                ; если да, то обрабатываем
    cmp byte[rdi], '+'          ; теперь сравниваем с плюсом
    jne parse_uint              
    .positive:                 
        inc rdi                 
        call parse_uint         
        inc rdx                
        ret
    .negative:
        inc rdi                 
        call parse_uint
        neg rax                 
        inc rdx
	ret 

;работает
string_copy:
    call string_length              
    cmp rdx, rax                    ; сравниваем буфер и нашу строку по длине
    jbe .else                      
    mov rcx, rax                   
    .loop:
        test rcx, rcx               ; проверка спец. символа
        je .end                 
        dec rcx                     
        mov dl, byte [rdi + rcx]    
        mov byte [rsi + rcx], dl
        jmp .loop
    .end:
        mov byte [rsi + rax], 0
        ret
    .else:
        xor rax, rax
        ret   
